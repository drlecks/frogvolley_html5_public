GroundClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	zindex: 2,
    image:null,
	name: "ceil",

    init: function (x, y, settings)  {
        this.parent(x, y, settings);
        
        if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width;
			this.size.y = this.image.height;
		}

		this.name = settings.name;

		var startPos = {
            x: (x+this.size.x*0.5)/1000,
            y: (y+this.size.y*0.5)/1000
        };
        // Create our physics body;
        var entityDef = {
            id: "Ground",
            type: 'static',
            x: startPos.x,
            y: startPos.y,
            halfHeight: 10/1000,
            halfWidth: gGameEngine.W*2/1000,
            damping: 0,
            angle: 0,
            userData: {
                "id": "Ground",
                "ent": this
            }
        };
		

		if(settings.zindex) this.zindex = settings.zindex;

		this.physBody = gPhysicsEngine.addBody(entityDef);
        this.physBody.SetLinearVelocity(new Vec2(0, 0));

		this.pos = startPos;
    },

    //-----------------------------------------
    kill: function () {
        // Destroy me as an entity
        this._killed = true;
    },

	update:function()
	{
		if(this.physBody)
		{
			//this.pos = this.physBody.m_xf.position;
		}
	},

	draw:function()
	{
		/*ctx.fillStyle = "green";
		ctx.fillRect(this.pos.x, this.pos.y, 480, 20);
		ctx.strokeStyle = "black";
		ctx.lineWidth = 2;
		ctx.strokeRect(this.pos.x, this.pos.y, 480, 20);*/
	},

	onTouch:function(body, algo, impulse)
	{
	}


});

gGameEngine.factory['Ground'] = GroundClass;