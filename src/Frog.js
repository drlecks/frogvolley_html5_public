FrogClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	move_speed: 100,
	jump_speed: 120,
	jumping : false,
	falling : false,
	animation_faling : false,
	TOCS:0,
	frog_num:0,
	run_right: false,
	run_left: false,

	//variables de nivell
	X_SPEED : 100,
	X_OFFSET : +15,
	move_lag : 0,
	LEVEL : 1,
	
	animation: null,


	polygon1: [
			new Vec2(0.65*60/1000, 0.35*45/1000),
			new Vec2(-0.6*60/1000, 0.35*45/1000),
			new Vec2(-0.6*60/1000, -0.6*45/1000),
			new Vec2(-0.2*60/1000, -0.9*45/1000),
			new Vec2(+0.2*60/1000, -0.9*45/1000),
			new Vec2(0.65*60/1000, -0.6*45/1000),
		  ],

    init: function (x, y, settings)  {

        this.parent(x, y, settings);
        
		this.loadImage(settings, x, y);

		var startPos = this.loadImage(settings, x, y);

        // Create our physics body;
        var entityDef = {
            id: "Frog",
            type: 'dynamic',
            x: startPos.x/1000,
            y: startPos.y/1000,
            halfHeight: 91*0.35/1000,
            halfWidth: 120*0.35/1000,
            damping: 1,
			mass: 1000,
            angle: 0,
            userData: {
                "id": "Frog",
                "ent": this
            },
			shape:"polygon",
			vertex:this.polygon1,
        };

		if(settings.zindex) this.zindex = settings.zindex;

		this.physBody = gPhysicsEngine.addBody(entityDef);

		this.setSpeed( 0,0 );
		this.pos = startPos;

		this.frog_num = settings.frog_num;
    },

    //-----------------------------------------
    kill: function () {
        // Destroy me as an entity
        this._killed = true;
    },

	update:function()
	{

		if(this.physBody)
		{
			this.pos.x = this.physBody.GetPosition().x*1000;
			this.speed.x = this.physBody.GetLinearVelocity().x*1000;
			this.pos.y = this.physBody.GetPosition().y*1000;
			this.speed.y = this.physBody.GetLinearVelocity().y*1000;
		}
		
		if(this.frog_num == 1)
		{
			if( gInputEngine.actions['move-left'] ) this.runLeft(this.move_speed);
			if( gInputEngine.actions['move-right'] ) this.runRight(this.move_speed);
			if( gInputEngine.actions['move-up'] ) this.jump();

			if(!game_mode)
			{
				if( gInputEngine.actions['move-left2'] ) this.runLeft(this.move_speed);
				if( gInputEngine.actions['move-right2'] ) this.runRight(this.move_speed);
				if( gInputEngine.actions['move-up2'] ) this.jump();
			}
		}
		else
		{
			if(game_mode)
			{
				if( gInputEngine.actions['move-left2'] ) this.runLeft(this.move_speed);
				if( gInputEngine.actions['move-right2'] ) this.runRight(this.move_speed);
				if( gInputEngine.actions['move-up2'] ) this.jump();
			}
			else
			{
				this.inteligenciaFrog();
			}
		}
	
		if(Math.abs(this.speed.x)<5)
		{
			this.run_right = false;
			this.run_left = false;
			if(this.animation!=null && (this.animation.type=="run_left"||this.animation.type=="run_right") )
				this.animation = null;
		}

		if(this.isJumping() && !this.falling && this.pos.y>gGameEngine.H-60 && this.speed.y>0)
		{
			this.animate(15,19,false,"fall");
			this.falling = true;
		}

		if(this.TOCS > gGameEngine.MAX_TOCS) gGameEngine.nouPunt( this.frog_num%2+1 ); 
	},
	
	animate:function(s,e,l,t)
	{
		if(this.animation == null || this.animation.type!= t)
		{
			this.animation = new AnimationClass(s,e,l,t);
		}
	},

	draw:function()
	{
		if(this.image!=null)
		{
			var frame = this.frames[0];
			if(this.animation!=null)
			{
				frame = this.frames[this.animation.currentFrame];
				if(this.animation.update()) this.animation = null;
			}

			this.drawFrameResized(this.image,this.pos.x,this.pos.y,frame);
		}

		
	},
	
	onTouch:function(body, algo, impulse)
	{
		if(body.m_userData.ent instanceof GroundClass) if(this.isJumping() && this.falling)this.endJumping();
		
	},

	moveTo: function(where)
	{
		if(this.pos.x>where)	this.runLeft(this.move_speed);
		else					this.runRight(this.move_speed);
	},

	runRight:function(speed)
	{
		if(speed > this.X_SPEED) speed = this.X_SPEED;
		
		if( (this.frog_num==1 && this.pos.x+this.size.x*0.5<gGameEngine.W*0.5-5) ||
			(this.frog_num==2 && this.pos.x+this.size.x*0.5<gGameEngine.W) )
		{
			if(!this.isJumping())
			{
				if(!this.run_right)
				{
					run_right = true;
					run_left = false;
					//animate(1, 4, 60, 1, true);
				}
			}

			this.setSpeed(speed,this.speed.y);

			if(!this.jumping)this.animate(0,4,true, "run_right");
		}
	},
	
	runLeft: function (speed)
	{
		if(speed > this.X_SPEED) speed = this.X_SPEED;
			
		if( (this.frog_num==1 && this.pos.x-this.size.x>0) || 
			(this.frog_num==2 && this.pos.x>gGameEngine.W*0.5+5) )
		{
			if(!this.isJumping())
			{
				if(!this.run_left)
				{
					this.run_left = true;
					this.run_right = false;
				}
			}
			this.setSpeed(-speed,this.speed.y);
			if(!this.jumping)this.animate(5,9,true, "run_left");
		}
	},

	jump: function()
	{
		//if(getSpeedY()==0 && MainActivity.SOUND) GameScene.sonidoSalt.play();
		
		if(!this.isJumping())
		{
			this.startJumping();
			this.setSpeed(this.speed.x,-this.jump_speed);

			this.animate(10,14,false, "jump");
			this.animation.keepLast(true);

			playSoundInstance('assets/Music/boing.mp3',0.5);
		}
	},
	
	endJumping: function()
	{
		this.jumping = false;
	},
	
	startJumping: function()
	{
		this.jumping = true;
		this.falling = false;
	},
	
	isJumping: function()
	{
		return this.jumping;
	},

	borrarToc: function()
	{
		this.TOCS = 0;
	},

	inteligenciaFrog: function()
	{
		var ball = gGameEngine.ball;
		var used_speed = this.X_SPEED;
		//decrease speed if jummping
		if( this.isJumping() && this.X_SPEED>1 ) used_speed = this.X_SPEED/2;
		
		if(ball.pos.x >= gGameEngine.W*0.5)
		{	
			//set time lag
			if(this.move_time == 0) this.move_time =  (Date.now());		
			var time_elapsed = (Date.now()) - this.move_time;
						
			//if lag time has passed
			if(this.move_lag==0 || this.move_lag < this.time_elapsed)
			{
				//go to ball
				if( ball.pos.x > this.pos.x - this.X_OFFSET )
				{
					if(ball.pos.x<gGameEngine.W - this.X_OFFSET*1.8)
					{
						this.runRight(used_speed);
					}
					else
					{
						if(this.pos.x>gGameEngine.W - this.X_OFFSET -15)
						{
							this.runLeft(used_speed);
						}
						else if(this.pos.x<gGameEngine.W - this.X_OFFSET)
						{
							this.runRight(used_speed);
						}
					}
				}
				else if( ball.pos.x < this.pos.x - (this.X_OFFSET+15) )
				{
					this.runLeft(used_speed);
				}
				
				//if limit reached
				//if(getX() + getWidth() > GameScene.FINESTRA_X) 	setX(GameScene.FINESTRA_X - getWidth());
				//if(getX() < GameScene.FINESTRA_X/2) 			setX(GameScene.FINESTRA_X/2 + 2);
				
				//if ball close to jump
				if (	ball.pos.y <= this.pos.y-80 && ball.pos.y >= this.pos.y-85	)
				{
					if(!this.isJumping())this.jump();
				}
			}
		}else
		{
			/*//if the ball is in the other side
			if(LEVEL!=1)
			{
				if(getX()+getWidth()/2 < GameScene.FINESTRA_X*3/4 -X_SPEED){
					
					runRight(used_speed);
				}
				else if(getX()+getWidth()/2 > GameScene.FINESTRA_X*3/4 +X_SPEED){
					
					runLeft(used_speed);
				}
			}*/
			
			this.move_time = 0;
		}
		
		return 1;
	},


});

gGameEngine.factory['Frog'] = FrogClass;