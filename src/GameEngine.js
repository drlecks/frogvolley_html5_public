

GameEngineClass = Class.extend({

    entities: [],
    factory: {},
    _deferredKill: [],
	frameRate: 60,
	statsdoob:null,
	W:0,
	H:0,
	background:null,
	frog1:null,
	frog2:null,
	ball:null,
	net:null,
	ground:null,
	floor:null,
	point_wall:null,
	end_wall:null,
	tronc_wall:null,
	number1:null,
	number2:null,
	time_text:null,
	pause:null,
	pause_menu:null,
	sound:null,
	

	BALL_EXTRA_SPEED: 0,
	PUNTS_1: 0,
	PUNTS_2: 0,
	MAX_PUNTS: 5,
	LAST_POINT: 0,
	
	start_time: 0,
	time: 0,
	MAX_TOCS: 3,
	ULTIM_TOC: 0,
	
	PAUSE: false,	
	
	STATE_POINT: 1,
	STATE_PAUSE: 2,
	STATE_PLAY: 3,
	STATE_END: 4,
	STATE_TRUE_END: 5,
	
	game_state: 0,
	next_state: 0,

    //-----------------------------
    init: function () {
		this.game_state = this.STATE_PLAY;
		this.next_state = this.STATE_PLAY;
	},

    //-----------------------------
    setup: function () {

		this.start_time = new Date().getTime();
		
		this.W = 480;
		this.H = 320;
        // Create physics engine
        gPhysicsEngine.create();

		gInputEngine.setup();
		//gMap = new TILEDMapClass();

        // Add contact listener
        gPhysicsEngine.addContactListener({

            PostSolve: function (bodyA, bodyB, impulse) {
                var uA = bodyA ? bodyA.GetUserData() : null;
                var uB = bodyB ? bodyB.GetUserData() : null;

                if (uA !== null) {
                    if (uA.ent !== null && uA.ent.onTouch) {
                        uA.ent.onTouch(bodyB, null, impulse);
                    }
                }

                if (uB !== null) {
                    if (uB.ent !== null && uB.ent.onTouch) {
                        uB.ent.onTouch(bodyA, null, impulse);
                    }
                }
            }
        });
		
		var settings = {};
		settings.image = gSimpleImage['back'];
		this.background = this.spawnEntity('NormalBack',0,0,settings);

		settings = {};
		settings.image = gSimpleImage['net_picture'];
		settings.zindex = 2;
		settings.resize = 0.5;
		this.net = this.spawnEntity('Net',this.W/2,this.H/2+100,settings);

		settings = {};
		settings.image = gSimpleImage['frog'];
		settings.zindex = 2;
		settings.resize = 1;
		settings.sprite = new Vec2(5,4);
		settings.frog_num = 1;
		this.frog1 = this.spawnEntity('Frog',this.W*0.25,this.H-50,settings);

		settings = {};
		settings.image = gSimpleImage['frogvs'];
		settings.zindex = 2;
		settings.resize = 1;
		settings.sprite = new Vec2(5,4);
		settings.frog_num = 2;
		this.frog2 = this.spawnEntity('Frog',this.W*0.75,this.H-50,settings);

		settings = {};
		settings.zindex = 2;
		settings.name = "floor";
		this.ground = this.spawnEntity('Ground',0,this.H-0,settings);

		settings = {};
		settings.zindex = 2;
		settings.name = "ceil";
		this.floor = this.spawnEntity('Ground',0,-30,settings);

		settings = {};
		settings.zindex = 2;
		this.ground = this.spawnEntity('Wall',this.W+20,0,settings);

		settings = {};
		settings.zindex = 2;
		this.ground = this.spawnEntity('Wall',-20,0,settings);

		settings = {};
		settings.image = gSimpleImage['ball'];
		settings.zindex = 2;
		var newsp = (Math.random()>0.5)?100:-100;
		settings.speed = new Vec2(newsp, 0);
		this.ball = this.spawnEntity('Ball',this.W/2,0,settings);

		settings = {};
		settings.image = gSimpleImage['tronc'];
		settings.zindex = 4;
		settings.resize = 0.75;
		this.tronc_wall = this.spawnEntity('Cartell',this.W/2,35,settings);
		this.tronc_wall.is_show = true;

		settings = {};
		settings.image = gSimpleImage['point'];
		settings.zindex = 4;
		settings.resize = 0.75;
		settings.special = "point";
		this.point_wall = this.spawnEntity('Cartell',this.W/2,this.H/2-30,settings);
		
		settings = {};
		settings.image = gSimpleImage['end'];
		settings.zindex = 4;
		settings.resize = 0.75;
		settings.special = "end";
		this.end_wall = this.spawnEntity('Cartell',this.W/2,this.H/2-30,settings);

		
		settings = {};
		settings.text = "00";
		settings.zindex = 4;
		settings.font = "40px Boogaloo";
		settings.has_stroke = false;
		settings.color = "#33cc00";
		this.number1 = this.spawnEntity('Text',60,50,settings);

		settings = {};
		settings.text = "00";
		settings.zindex = 4;
		settings.font = "40px Boogaloo";
		settings.has_stroke = false;
		settings.color = "#33cc00";
		this.number2 = this.spawnEntity('Text',this.W-100,50,settings);

		settings = {};
		settings.text = "00:00";
		settings.zindex = 4;
		settings.font = "14px Boogaloo";
		settings.has_stroke = false;
		settings.color = "#ffffff";
		this.time_text = this.spawnEntity('Text',this.W/2-16,42,settings);


		settings = {};
		settings.image = gSimpleImage['menu_info'];
		settings.zindex = 10;
		settings.resize = 0.75;
		settings.special = "pause";
		this.pause = this.spawnEntity('Cartell',15,15,settings);
		this.pause.is_show = true;


		settings = {};
		settings.image = gSimpleImage['pause_menu'];
		settings.zindex = 10;
		settings.resize = 0.75;
		settings.special = "pause_menu";
		this.pause_menu = this.spawnEntity('Cartell',this.W/2,this.H/2,settings);

		settings = {};
		settings.image = gSimpleImage['sound'];
		settings.zindex = 10;
		settings.resize = 0.75;
		settings.special = "sound";
		this.sound = this.spawnEntity('Cartell',this.W-15,15,settings);
		this.sound.is_show = true;

    },

	start: function()
	{
		this.statsdoob = new Stats();
		this.statsdoob.setMode(0); // 0: fps, 1: ms

		// Align top-left
		this.statsdoob.domElement.style.position = 'absolute';
		this.statsdoob.domElement.style.right = '0px';
		this.statsdoob.domElement.style.top = '0px';

		document.body.appendChild( this.statsdoob.domElement );
		updateintervalID = setInterval( function () {

			gGameEngine.statsdoob.begin();

			gGameEngine.update();
			gGameEngine.draw();

			gGameEngine.statsdoob.end();

		}, 1000 / this.frameRate );

		playSoundLoop('assets/Music/back_music.mp3');
	},

   spawnEntity: function (typename, x, y, settings) {
    var entityClass = gGameEngine.factory[typename];
    var es = settings || {};
    es.type = typename;
    var ent = new(entityClass)(x, y, es);

    gGameEngine.entities.push(ent);     
    return ent;
  },

    update: function () {

		if(!this.PAUSE)
		{
			for (var i = 0; i < gGameEngine.entities.length; i++) {
				var ent = gGameEngine.entities[i];
				if(!ent._killed) {
					ent.update();
				} else {
					gGameEngine._deferredKill.push(ent);
				}
			}

			for (var j = 0; j < gGameEngine._deferredKill.length; j++) {
				gGameEngine.entities.erase(gGameEngine._deferredKill[j]);
			}

			gGameEngine._deferredKill = [];

			// Update physics engine
			gPhysicsEngine.update();

			this.setTime();
		}
    },
	 draw: function () {
		ctx.clearRect(0,0,canvas.width, canvas.height);

        // Bucket entities by zIndex
        var fudgeVariance = 128;
        var zIndex_array = [];
        var entities_bucketed_by_zIndex = {};
        gGameEngine.entities.forEach(function(entity) {
                if(entities_bucketed_by_zIndex[entity.zindex]==null) entities_bucketed_by_zIndex[entity.zindex] = [];
                entities_bucketed_by_zIndex[entity.zindex].push(entity);
                zIndex_array.push(entity.zindex);
        });

        // Draw entities sorted by zIndex
        zIndex_array.forEach(function(zind) {
            entities_bucketed_by_zIndex[zind].forEach(function(entity){
               entity.draw(); 
            });
        });
    },

	nouPunt: function(guanyador)
	{
		if(this.game_state == this.STATE_PLAY)
		{
			this.frog1.borrarToc();
			this.frog2.borrarToc();			
			
			this.BALL_EXTRA_SPEED = 0;
			
			if(guanyador == 1){
				this.PUNTS_1++;
				this.LAST_POINT = 1;
				//if(MainActivity.SOUND) sonidoPunt.play();
				playSoundInstance('assets/Music/yeah.mp3');
			}
			else{
				this.PUNTS_2++;
				this.LAST_POINT = 2;
				//if(MainActivity.SOUND) sonidoDolent.play();
				playSoundInstance('assets/Music/buzzer.mp3');
			}
	
			this.number1.text = "0"+this.PUNTS_1;
			this.number2.text = "0"+this.PUNTS_2;

			if(this.PUNTS_1 >= this.MAX_PUNTS) 		this.setNextState(this.STATE_END);
			else if(this.PUNTS_2 >= this.MAX_PUNTS) this.setNextState(this.STATE_END);
			else 									this.setNextState(this.STATE_POINT);
			
		}

	},

	setNextState: function(next)
    {
    	this.game_state = next;
    	//console.log(this.game_state+": "+this.PUNTS_1+"-"+this.PUNTS_2);
			
    	if(this.game_state == this.STATE_PLAY)
    	{
			if(this.LAST_POINT==0) this.LAST_POINT = (Math.random(0,1)>0.5)?0:1;

			if(this.LAST_POINT == 1)
    			this.ball.setPosition( this.W*0.25, 50);
			else if(this.LAST_POINT == 2)
				this.ball.setPosition( this.W*0.75, 50);
			
			this.ball.setSpeed(0,0);
			this.ball.setRotation(0);
			this.frog1.borrarToc();
			this.frog2.borrarToc();
    	}
    	else if(this.game_state==this.STATE_POINT)
    	{
    		this.point_wall.show();
    	}
    	else if(this.game_state==this.STATE_END)
    	{
    		this.end_wall.show();
    	}
    	else if(this.game_state==this.STATE_TRUE_END)
    	{
			//gGameEngine.restartGame();
			endGame();
    	}
    	
    },

	setTime:function()
	{
		this.time = ( (new Date().getTime()) - this.start_time )/1000;

		var seg = Math.floor(this.time%60);
		if(seg<10) seg = "0"+seg;

		var min = Math.floor(this.time/60);
		if(min<10) min = "0"+min;

		this.time_text.text = min+":"+seg;
	},

	restartGame: function()
	{
		this.ball.setRotation(0);
		this.ball.setPosition(this.W/2,10);
		var newsp = (Math.random()>0.5)?100:-100;
		this.ball.setSpeed(newsp,0);

		this.BALL_EXTRA_SPEED = 0;
		this.PUNTS_1 = 0;
		this.PUNTS_2 = 0;
		this.LAST_POINT = 0;
		
		this.start_time = new Date().getTime();
		this.time = 0;
		this.ULTIM_TOC = 0;
		
		this.PAUSE = false;	
		
		this.game_state = 0;
		this.next_state = 0;

		this.point_wall.is_show = false;
		this.end_wall.is_show = false;

		this.game_state = this.STATE_PLAY;
		this.next_state = this.STATE_PLAY;

		this.number1.text = "00";
		this.number2.text = "00";
	},

});

gGameEngine = new GameEngineClass();

