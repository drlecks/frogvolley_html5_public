NetClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	zindex: 2,
    image:null,

    init: function (x, y, settings)  {
        this.parent(x, y, settings);
        
		if(settings.resize) this.resize = settings.resize;

		if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width*this.resize;
			this.size.y = this.image.height*this.resize;
		}

		var startPos = {
            x: x/1000,
            y: y/1000
        };

        // Create our physics body;
        var entityDef = {
            id: "Net",
            type: 'static',
            x: startPos.x,
            y: startPos.y,
            halfHeight: this.size.y*0.45/1000,
            halfWidth: this.size.x*0.25/1000,
            damping: 0,
            angle: 0,
            userData: {
                "id": "Net",
                "ent": this
            },
			useBouncyFixture:false,
			shape:"net"
        };
		


		if(settings.zindex) this.zindex = settings.zindex;

		this.physBody = gPhysicsEngine.addBody(entityDef);
		this.physBody.SetLinearVelocity( new Vec2(0, 0) );
    },

    //-----------------------------------------
    kill: function () {
        // Destroy me as an entity
        this._killed = true;
    },

	update:function()
	{

		if(this.physBody)
		{
			this.pos.x = this.physBody.m_xf.position.x*1000;
			this.pos.y = this.physBody.m_xf.position.y*1000;
		}
	},

	draw:function()
	{
		if(this.image!=null)
		{
			this.drawResized(this.image,this.pos.x-this.size.x/2,this.pos.y-this.size.y/2,this.resize);
		}
	},
	
	onTouch:function(body, algo, impulse)
	{
	}


});

gGameEngine.factory['Net'] = NetClass;