BallClass = EntityClass.extend({
    physBody: null,
    _killed: false,
	zindex: 2,
    image:null,
	impulsed:false,
	rotation_speed:0,

    init: function (x, y, settings)  {
        this.parent(x, y, settings);
        

		if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width;
			this.size.y = this.image.height;
		}

		var startPos = {
            x: x,
            y: y
        };

        // Create our physics body;
        var entityDef = {
            id: "Ball",
            type: 'dynamic',
            x: startPos.x/1000,
            y: startPos.y/1000,
            halfHeight: 26/1000,
            halfWidth: 26/1000,
			mass:1.0,
            damping: 0.0,
            angle: 0,
            userData: {
                "id": "Ball",
                "ent": this
            },
			useBouncyFixture:true,
			shape:"circle"
        };
		
		if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width;
			this.size.y = this.image.height;
		}

		if(settings.zindex) this.zindex = settings.zindex;

		this.physBody = gPhysicsEngine.addBody(entityDef);	

		this.setSpeed( settings.speed.x,settings.speed.y );
		this.pos = startPos;
    },

    //-----------------------------------------
    kill: function () {
        // Destroy me as an entity
        this._killed = true;
    },

	update:function()
	{
		if(this.physBody)
		{
			//this.protation += rotation_speed;

			this.pos.x = this.physBody.m_xf.position.x*1000;
			this.pos.y = this.physBody.m_xf.position.y*1000;
			this.rotation = this.physBody.GetAngle();
			this.speed.x = this.physBody.GetLinearVelocity().x*1000;
			this.speed.y = this.physBody.GetLinearVelocity().y*1000;

			this.physBody.SetAngle( this.physBody.GetAngle()+this.rotation_speed );
			this.rotation_speed *=0.98;
		}
	},

	draw:function()
	{
		if(this.image!=null)
		{
			this.drawRotatedImage(this.image,this.pos.x,this.pos.y, this.rotation);
		}
	},
	
	onTouch:function(body, algo, impulse)
	{
		
		if(body.m_userData.ent instanceof FrogClass)
		{
			if(this.impulsed) return;

			var frog = body.m_userData.ent;
			//movem la pilota
			var new_x = (this.pos.x - frog.pos.x)*3.9+1;
			var signe = Math.abs(new_x)/new_x;
			var ball_sx = this.speed.x + new_x + signe*gGameEngine.BALL_EXTRA_SPEED;
			
			var new_y = 1 - ( (this.pos.y - frog.pos.y) / this.size.y );
			var speedY = (new_y+0.1)*(-1)*Math.abs(this.speed.y) + this.speed.y - 30;
			
			var vector =  new Vec2( ball_sx, (speedY*(0.65) - gGameEngine.BALL_EXTRA_SPEED) );

			vector.x /= 1000;
			vector.y /= 1000;

			this.physBody.SetLinearVelocity( vector); 
			gGameEngine.BALL_EXTRA_SPEED+=5;

			this.impulsed = true;
			setTimeout(function(){gGameEngine.ball.impulsed=false;},200);
			
			if(gGameEngine.ULTIM_TOC != frog.frog_num)
			{
				gGameEngine.frog1.borrarToc();
				gGameEngine.frog2.borrarToc();
			}

			frog.TOCS++;
			gGameEngine.ULTIM_TOC = frog.frog_num;

			this.addRotation(vector.x);	

			playSoundInstance('assets/Music/hit.mp3');
		}
		else if(body.m_userData.ent instanceof GroundClass)
		{
			var floor = body.m_userData.ent;
			
			if(floor.name!="floor") return;

			if(this.pos.x<=gGameEngine.W/2) gGameEngine.nouPunt(2);
			else							gGameEngine.nouPunt(1);

			playSoundInstance('assets/Music/hit.mp3');
		}
		else if(body.m_userData.ent instanceof NetClass)
		{
			var net = body.m_userData.ent;

			if(this.pos.x<=gGameEngine.W/2 && Math.abs(this.speed.x)<1) 
				this.physBody.SetLinearVelocity( new Vec2(this.speed.x/1000-0.005, this.speed.y/1000) ); 
			else if(this.pos.x>gGameEngine.W/2 && Math.abs(this.speed.x)<1) 
				this.physBody.SetLinearVelocity( new Vec2(this.speed.x/1000+0.005, this.speed.y/1000) );

			playSoundInstance('assets/Music/net.mp3');
		}
		else{
			playSoundInstance('assets/Music/hit.mp3');
		}

		this.rotation_speed *= 0.9;
	}


});

gGameEngine.factory['Ball'] = BallClass;