CartellClass = EntityClass.extend({
	zindex: 4,
    image:null,
	is_show: false,
	special: "",

    init: function (x, y, settings)  {
        this.parent(x, y, settings);
        
		if(settings.resize) this.resize = settings.resize;
		if(settings.special) this.special = settings.special;

		if(settings.image)
		{
			this.image = settings.image;
			this.size.x = this.image.width*this.resize;
			this.size.y = this.image.height*this.resize;
		}

		var startPos = {
            x: x,
            y: y
        };

		if(settings.zindex) this.zindex = settings.zindex;
		this.pos = startPos;
    },

	update:function()
	{
	},

	draw:function()
	{
		if(this.image!=null && this.is_show)
		{
			this.drawResized(this.image,this.pos.x-this.size.x/2,this.pos.y-this.size.y/2,this.resize);
		}
	},
	
	onTouch:function(body, algo, impulse)
	{
	},

	show: function()
	{
		this.is_show = true;

		if(this.special=="point")
		{
			setTimeout(function(){
				gGameEngine.point_wall.is_show = false;
				gGameEngine.setNextState(gGameEngine.STATE_PLAY);
			},3000);
		}
		else if(this.special=="end")
		{
			setTimeout(function(){
				gGameEngine.setNextState(gGameEngine.STATE_TRUE_END);
			},4000);
		}
	},


});

gGameEngine.factory['Cartell'] = CartellClass;